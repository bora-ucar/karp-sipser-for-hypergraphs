#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include "ksmd_utils.h"
#define MAXCHAR 100
double u_seconds(void)
{
    struct timeval tp;
    gettimeofday(&tp, NULL);
    return (double) tp.tv_sec + (double) tp.tv_usec / 1000000.0;    
}

int verify_matching(int n,int *match,int nmatches,int k,int *h_points,int *h_begs,int *match_set)
{	
	int correct=1;
	int  nmatch=0;
	int *count_matches=calloc(n,sizeof(int));
	for (int i=0;i<n;++i){
		int x=match[i];
		if (x>=0){
			nmatch++;
			count_matches[x]++;		
			if (count_matches[x]==2){
				correct=0;
				break;							
			}	
		}	
	}
	if (correct && nmatch/k==nmatches){
		for (int i=0;i< nmatches;++i){
			for (int z=0;z<k;++z){
				count_matches[h_points[h_begs[match_set[i]]+z]]--;
				if (count_matches[h_points[h_begs[match_set[i]]+z]]<0){
					correct=0;
					break;	
				}	
			}
		}						
	}else{
		correct=0;
	}
	free(count_matches);
	return correct;
}

int test_ksmd(char *s,int o,int **match,int *nov,int *matches){
        double t0,t1;
	int n, m,i,k,min_ni,correct_match=1;
	int ans;
 	int *v_hids, *v_starts, *h_begs, *h_points;
	int *edge_list;
	int *match_set;
	t0=u_seconds();	
	read_file_ks(&n,&m,&k,s, &v_hids, &v_starts, &h_begs, &h_points,o, &min_ni);
	match_set=malloc( min_ni * sizeof(int));
	*match=malloc(n * sizeof(int));
	edge_list=malloc(m * sizeof(int));
	for (i=0;i<m;++i) edge_list[i]=i;	
		
	ans= ksmd(edge_list,n, m, v_hids, v_starts, h_begs, h_points,match,&match_set);
	t1=u_seconds();
	printf("Karp-Sipser-mindegree: matching found: %d, Time elapsed: %.4f seconds \n",ans, (t1-t0));
	correct_match=verify_matching(n,*match,ans, k,h_points,h_begs,match_set);
	if (!correct_match){
		printf("Matching is not correct \n");	
	}
	*nov=n;
	*matches=ans;
	free(v_hids);
	free(v_starts);
	free(h_begs);
	free(h_points);
	free(edge_list); 
	return ans;
}
int main(int argc, char **argv){
	 char *filename=argv[1];
	int o=atoi(argv[2]);
	int *match,n,matches;
	test_ksmd(filename,o,&match,&n,&matches);
	free(match);
	return 0;
}
