#ifndef KSMD_UTILS_H
#define KSMD_UTILS_H

void updateDegs(int h, int *v_hids, int *v_starts, int *h_begs, int *h_points, int *matched, int **degs,int **d1lst, int *_d1st, int *_d1end,int *active);
int ksmd(int *edge_list, int  n, int m, int *v_hids, int *v_starts, int *h_begs, int *h_points,int  **match,int **match_set);
void read_file_ks(int *n,int *m,int *k,char *filename, int **v_hids, int **v_starts, int **h_begs, int **h_points,int o,int  *min_ni);

#endif
