function R2sup(fname,n,k)
   
    m= n * (n-1)/2 + 2;
    o=1;
    T=zeros(m,k);
    for i = 1 : n
		for j= i+1 : n
			T(o,1)=i-1;
			for z= 2 : k
				T(o,z)=j-1;
			end
			o=o+1;
		end
	end
	T(o,1)=1;
	for z= 2 : k
				T(o,z)=0;
			end
	o=o+1;
	T(o,1)=n;
	for z= 2 : k
				T(o,z)=n-1;
			end
    saveT(fname,T,n);
end
