function saveT(fname,T,n)
    fileID=fopen(fname,'w');
    m=size(T,1);
    k=size(T,2);
    fprintf(fileID,'%d %d %d \n',n,m,k);
    for i=1:m
        for j=1:k
            fprintf(fileID,'%d ',T(i,j));
        end
        fprintf(fileID,'\n');
    end
end