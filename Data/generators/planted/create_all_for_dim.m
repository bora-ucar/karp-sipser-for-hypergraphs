function create_all_for_dim(ndims)
	mkdir(strcat('tests/',int2str(ndims)));
    fileSID=fopen(strcat('tests/',int2str(ndims),'/summary.txt'),'w');
    siz=[4000 8000];
    choice=[2 4 6 8];
    for i=1: length(siz)
        for j=1:length(choice)
            n=siz(i);
            k= choice(j);
            for z=1:10
                T=createRandomHyperGraph(n,ndims,(k-1)*n);
                fname=strcat(int2str(n),'_',int2str(k),'_',int2str(z),'a.txt');
                fprintf(fileSID,'%s \n',fname);
                fname2=strcat('tests/',int2str(ndims),'/',fname);
                saveT(fname2,T,n);
                
                T1=zeros(k*n,ndims);
                for a=1:k*n
                    for b=1:ndims
                        if a <= (k-1)*n
                            T1(a,b)=T(a,b);
                        else
                            T1(a,b)= a - (k-1)*n - 1;
                        end
                    end
                end
                T1=T1(randperm(k*n),:);
                fname=strcat(int2str(n),'_',int2str(k),'_',int2str(z),'b.txt');
                fprintf(fileSID,'%s \n',fname);
                fname2=strcat('tests/',int2str(ndims),'/',fname);
                saveT(fname2,T1,n);
            end
        end
    end
    fclose(fileSID);
    end
