function create_all_for_dim(ndims)
    	mkdir(strcat('dims/',int2str(ndims)));
    fileSID=fopen(strcat('dims/',int2str(ndims),'/summary.txt'),'w');
    size=[1000,5000,10000];
    choice=[2 4 8];
    if ndims==6 
        choice=[2 4 8 16];
    elseif ndims==9 
        choice=[2 4 8 16 32];
    elseif ndims==12 
        choice=[2 4 8 16 32 44];
    end
    for i=1: length(size)
        for j=1:length(choice)
            n=size(i);
            k= choice(j);
            for z=1:10
                hedges = koutHypergraphs(n, ndims,k);
                fname=strcat(int2str(n),'_',int2str(k),'_',int2str(z),'.txt');
                fprintf(fileSID,'%s \n',fname);
                fileID = fopen(strcat('dims/',int2str(ndims),'/',fname),'w');
                fprintf(fileID,'%d %d %d\n',n,length(hedges),ndims);
                for a=1:length(hedges)
                    for b=1: ndims
                        fprintf(fileID,'%d ', (hedges(a,b)-1));
                    end
                    fprintf(fileID,'\n');
                end
                fclose(fileID);
            end
        end
    end
    fclose(fileSID);
    end
