function create_all_for_dim3
	mkdir('tests/');
    fileSID=fopen(strcat('tests/summary.txt'),'w');
    siz=[300];
    choice=[2 4 8 16 32];
    for i=1: length(siz)
        for j=1:length(choice)
            n=siz(i);
            k= choice(j);
                B=ksWcase(n,k);
                [ii,jj,~]=find(B);
                nnzB=nnz(B);
                nnzT=nnzB*n;
                fname=strcat(int2str(n),'_',int2str(k),'.txt');
                fprintf(fileSID,'%s \n',fname);
                 fname2=strcat('tests/',fname);
                fileID=fopen(fname2,'w');
                fprintf(fileID,'%d %d %d \n',n,nnzT,3);
                for z= 1 : n
                    for l = 1 : nnzB
                    fprintf(fileID,'%d %d %d \n',ii(l)-1,jj(l)-1,z-1);
                    end
                end
                fclose(fileID);
               

        end
    end
    fclose(fileSID);
    end
