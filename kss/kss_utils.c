#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include  "kss_utils.h"
struct  sortb_kss{
	unsigned long  val;
	int pos;
};
int cmp_kss(const void *s1, const void *s2)
{
  struct sortb_kss *e1 = (struct sortb_kss*)s1;
  struct sortb_kss *e2 = (struct sortb_kss*)s2;
  return e1->val - e2->val;
}

//This function reads an (n x .. x n) K-D tensor from a.txt
//The first two numbers are n and number of observations/nonzeros inside
void read_tensor_kss(char *filename,int *n,int *m, int *k,int **edge_list,int **edge_order, int **degs,int **dim_starts,int **dim_ends,int o){
  FILE *in_file=fopen(filename, "r");
  int t_npk=0,t_m, t_k,kv, i,j,t_n,temp,cx;
  if (o==0) 
	fscanf(in_file, "%d", &t_npk);
  fscanf(in_file, "%d", &t_m);
  fscanf(in_file, "%d", &t_k);
  *dim_starts=(int *)calloc(t_k,sizeof(int)); //first ID of a vertex in i-th dimension
  *dim_ends=(int *)calloc(t_k,sizeof(int)); //first ID of a vertex in i-th dimension
  *edge_order= (int *)malloc(t_m *sizeof(int));
  t_n= 0;
  *edge_list = (int *)malloc(t_m * t_k*sizeof(int)); //the tensor will be stored in coordinate format ( m  x k list)
 (*dim_starts)[0]=0; //first dimensions start at zero of array order
  temp=t_npk;
	if (o==1) //o==1 if random random sizes allowed
	  fscanf(in_file,"%d",&temp); //in this case, read size
	t_n+=temp;
   	(*dim_ends)[0]= temp - 1;//where it inits
	  for (i=1;i<t_k;++i){
		temp=t_npk;
		if (o==1)
	  		fscanf(in_file,"%d",&temp); //in this case, read size
		t_n+=temp;
		(*dim_starts)[i]= (*dim_ends)[i-1] + 1; //and any other dimension starts immediatelly from where the previous finish
		(*dim_ends)[i] =  (*dim_starts)[i] + temp-1;   
	}
	   *degs= (int*)calloc(t_n,sizeof(int));
  for (i=0; i<t_m;i++) {
	(*edge_order)[i] =  i;
  }
  for (i=0;i<t_m;++i){ //for each hyperedge
    for (j=0;j<t_k;++j){ //read its endpoints one by one
        fscanf(in_file,"%d",&kv);
        cx = (*dim_starts)[j] + kv;
		(*edge_list)[i*t_k+j]= cx;
		(*degs)[cx]++; 
  }
}
  *n=t_n;
  *m=t_m;
  *k=t_k;
   fclose(in_file);
}

//This is sinkhorn-knopp on a K-D tensor
//Coeffs has scaling coefficient for each node
//n - number of nodes, m - nonzeros
//steps - no iterations, error is max error
//m_val is maximum value in the scaled tensor, and h_pos is the  ID of the hyper-edge selected
//k is the number of dimensions
//matched: matched node or not
//order contains an ordered list of  unmatched nodes
//on :  the initial value of n
//Other - double precision is used
void  sk_one( int *edge_list, int *edge_order, double **coeffs,int m,int steps,double *m_val, int *h_pos,int k,int *degs, int max_ni, double *S,int offset,int *dim_starts,int *dim_ends,int *order){
  int iter=0,q,i;
  //extra var
  double val;
  double temp_val;
  int sc_div;
   int order_b,order_e;
  int   nj;
  *m_val=-1;
  *h_pos=-1;
   
  for (iter=0; iter < steps; ++iter){
		sc_div =  (offset+iter) %  k; //find dimension
		order_b = dim_starts[sc_div]; //fix beginning & end
		order_e = dim_ends[sc_div];
		for (i= order_b; i<=order_e;++i) 
			S[order[i]]=0; //set all sums initially zero
		for (q=0;q<m;++q){ //iterate over edges
			temp_val=1.0;
			for (i=0;i<k;++i){ //for each endpoint of hyperedge
				if (i!= sc_div){
						temp_val *= (*coeffs)[edge_list[edge_order[q]*k+i]]; //if different dimension than scaling, multiply
				}
			}
				S[edge_list[edge_order[q]*k+sc_div]]+=temp_val;  //add to sum
		}
		//scale:
		order_b = dim_starts[sc_div]; //fix beginning & end
		order_e = dim_ends[sc_div];
		 nj=  order_e - order_b + 1;
    	for (i= order_b; i<=order_e;++i) {
			(*coeffs)[order[i]] = (max_ni/nj)/ S[order[i]];
		}
  } //The scaling part is in fact over, we need to compute the error
		for (q=0;q<k;++q) {
			order_b =   dim_starts[q];
			order_e=  dim_ends[q];
			for (i= order_b; i<=order_e;++i)
			S[order[i]]=0; //set scale error for each node to 0
		}
		//go over all the nonzeros
		for (q=0;q<m;++q){
			val=1.0;
			for (i=0;i<k;++i){ //multiply all nodes together
				val*=(*coeffs)[edge_list[edge_order[q]*k+i]];
			}
			//store the maximum value and the coordinates
			if (val > *m_val){
				*m_val=val;
				*h_pos = edge_order[q];
			}
		}
		
}

//this removes the - dimensions  which were matched - (as well as their connected hyperedges) from the tensor
void split_ijz(int *edge_list,int **edge_order,int *m,int *matched, int k,int **degs,int deg_limit) {
  int c_edge = 0, i,remain,cc_edge;
  int last_edge = *m-1;

  //c_edge is current edge
  while (c_edge  <= last_edge  ){
		remain=1;
		cc_edge=(*edge_order)[c_edge];
		for (i=0;i<k;++i)  {
			if (matched[edge_list[cc_edge*k+i]] != -1) {
				remain=0;
				break;
			}
		}
		//if this hyperedge remains
    if (remain){
		c_edge++;
    }else{ //otherwise swap this one and the last one 
	    // and if we keep the edge_list order as is we maybe having to iterate over all m (instead for example   m/2) to find the good edges to scale.So swapping helps us ensure that we always know where the good edges are)
		for (i=0;i<k;++i){
			(*degs)[edge_list[cc_edge*k+i]]--; //first reduce degs
			if (matched[edge_list[cc_edge*k+i]]!= -1){
				deg_limit--;
			}
			
		}
		(*edge_order)[c_edge] = (*edge_order)[last_edge];
		last_edge--;
    }
  }
  *m =  last_edge + 1;
}
void split_ijz_2(int **edge_list,int **edge_order,int *m,int h1,int h2,int k,int **degs,int r2a,int r2b,int index_r, int *order, int *dim_starts, int *dim_ends,int **S_stack,int *c_stack) {
   int c_edge = 0, i,j, remain, count_eq,cc_edge;
  int last_edge = *m-1;
  int da= (*degs)[r2a]-1;
  int db = (*degs)[r2b]-1;
  struct sortb_kss  *sorts= (struct sortb_kss *)malloc( (da+db + 5) * sizeof(struct sortb_kss));
  int t_stack = *c_stack;
  int ti_i=0;
  int z;
  unsigned long t_temp;
  unsigned long t_val;

  (*edge_list)[h1*k+1]=-(*edge_list)[h1*k+1];
  (*edge_list)[h2*k+1]=-(*edge_list)[h2*k+1];
  (*degs)[r2b]--;
   (*S_stack)[t_stack] = -( index_r + 1); t_stack++;
  while (c_edge <= last_edge){
		remain=1;
		cc_edge=(*edge_order)[c_edge];
		if ((*edge_list)[cc_edge*k+1] < 0) { //we do not lose edges here, we just remove the 2 edges
			remain=0;
			(*edge_list)[cc_edge*k+1] = - (*edge_list)[cc_edge*k+1] ; //revert it to original state!
		}
		//if this hyperedge remains
   		 if (remain){
			if ((*edge_list)[cc_edge*k+index_r]==r2a){ //time to transfer r2a-->r2b
				  (*edge_list)[cc_edge*k+index_r]=r2b;
				  (*S_stack)[t_stack]= cc_edge;  t_stack++;
				  (*degs)[r2b]++; //and increase the degree of r2b
			}
			if ((*edge_list)[cc_edge*k+index_r]==r2b){
				t_temp=0;
				t_val=0;
			 	for (i=0;i<k;++i){ //calculate the hash-value
					t_temp=(*edge_list)[cc_edge*k+i];
					t_val+= t_temp*t_temp;
			 	}
				sorts[ti_i].val=t_val;
				sorts[ti_i].pos=cc_edge;
      				ti_i++;
				}
		}
	     if (!remain){
      		(*edge_order)[c_edge]=  (*edge_order)[last_edge];//do exchange
     		last_edge--;
    	}else{
			c_edge++;
    	}
  }
        qsort(sorts, ti_i, sizeof(struct sortb_kss), cmp_kss);
		for (i=0;i<ti_i;++i){
			remain=1;
			for (j=i+1;j<ti_i;++j){
				count_eq=0;
				if (sorts[j].val != sorts[i].val)
					break;
				else{
					for (z=0;z<k;++z){
						if  ((*edge_list)[sorts[i].pos*k+z] ==(*edge_list)[sorts[j].pos*k+z]){
							count_eq++;
						}
					}
					if (count_eq==k){
						remain=0;
						break;
					}
				}
			}
			if (remain==0){
				(*edge_list)[sorts[i].pos*k+1]=-(*edge_list)[sorts[i].pos*k+1];
			}
		}
	 c_edge=0;
	 while (c_edge <= last_edge){
		 cc_edge=(*edge_order)[c_edge];
		if ((*edge_list)[cc_edge*k+1]< 0 ) { //edge is removed, but also bring it back to original state
			 (*edge_list)[cc_edge*k+1]=-(*edge_list)[cc_edge*k+1];
			 (*degs)[r2b]--;
			 for (i=0;i<k;++i){
				 if (i != index_r){
					(*degs)[(*edge_list)[cc_edge*k+i]]--;
				}//then replace
      		}
      		 (*edge_order)[c_edge]=(*edge_order)[last_edge];
     			last_edge--;
		}else{
			c_edge++;
		}
  }
  (*S_stack)[t_stack]= h1; t_stack++;
  (*S_stack)[t_stack]= h2; t_stack++;
  (*S_stack)[t_stack] = r2b; t_stack++;
	*c_stack = t_stack;
  *m =  last_edge + 1;
  free(sorts);
}
void  ks_red_1( int *edge_list, int *edge_order, double **coeffs,int m,int k,int **inactive,int **matched, int *order,int *degs,int d0_starts,int d0_ends,int *rule_nums, double *S,int max_ni,int **edge_ids,int *deg_limit){
	  int c_r=0,i;
	  int q=0;
	  double temp_val;
	  int d_l=0;
	  int count_d1;
	 for (i= d0_starts; i<=d0_ends;++i) S[order[i]]=0; //set all sums initially zero
	  for (q=0;q<m;++q){ //iterate over edges
			temp_val=1.0;
			count_d1=0;
			if (c_r == 0 ){ //if no rule applied as of now, do the scaling as usual
				for (i=0;i<k;++i){
					if ((*inactive)[edge_list[edge_order[q]*k+i]] > 0 ){ //check edge not matched in this function call
						temp_val=-1; //safecheck
						count_d1=-1;
						break;
					}
					if ( degs[edge_list[edge_order[q]*k+i]]==1) count_d1++;

					if (i!= 1){  //calculate sum for each distinct coordinate of the 1-st dimension
						temp_val *= (*coeffs)[edge_list[edge_order[q]*k+i]]; //if different dimension than scaling, multiply
					}
				}
				if (temp_val != -1)
					S[edge_list[edge_order[q]*k+1]]+=temp_val;  //add to sum

			}else{ //otherwise don't do scaling because multiplications are expensive!
				for (i=0;i<k;++i){
					if ( degs[edge_list[edge_order[q]*k+i]]==1) count_d1++;
					else{
						if ((*inactive)[edge_list[edge_order[q]*k+i]] > 0 ){
							count_d1=-1; //safe-case
							break; // this edge has a node, that was matched in a previous reduction!
						}
					}
				}
			}
			if (count_d1 >= k -1 ){ //degree-1 reduction possible				
				for (i=0;i<k;++i){ //now update the valid array, so that we don't get duplicate match
					(*matched)[edge_list[edge_order[q]*k+i]]= edge_order[q];
					(*edge_ids)[c_r]=edge_order[q];
					(*inactive)[edge_list[edge_order[q]*k+i]]=1;
					d_l+= degs[edge_list[edge_order[q]*k+i]];
				}
				c_r++;
			}

		}
		if (c_r == 0 ){ // if no reductions, used we should update coeffs for 1-st dimension
				 for (i= d0_starts; i<=d0_ends;++i)
					(*coeffs)[order[i]] =  (max_ni / (d0_ends - d0_starts + 1) )/ S[order[i]];
		}
			*rule_nums=c_r;
			*deg_limit=d_l;
}
void  ks_red_2( int *edge_list, int *edge_order, double **coeffs,int m,int k, int *order,int *degs,int d1_starts,int d1_ends,int *h_pos1,int *h_pos2, double *S,int max_ni, int *f_appear){
	 int e1,e2,i,q;
	 double temp_val;
	 int count_d2=0;
	 int count_eq=0;
	 int count_exeq=0;
	 for (i= d1_starts; i<=d1_ends;++i) S[order[i]]=0; //set all sums initially zero
	 *h_pos1=-1;
	 *h_pos2=-1;
	 for (q=0;q<m;++q){
				temp_val=1.0;
				count_d2=0;
				e1=-1;
				e2=-1;
				for (i=0;i<k;++i){ //for each endpoint of hyperedge
					if ( degs[edge_list[edge_order[q]*k+i]]<=2){//if it has degree-2 it is interesting to us
						count_d2++;
						if (degs[edge_list[edge_order[q]*k+i]]==2){
							if (f_appear[edge_list[edge_order[q]*k+i]]==-1){ //if first time it appears
								f_appear[edge_list[edge_order[q]*k+i]]=q;   //set it to q
							}else{ //maybe the other edge is what we want
						  	   if (e1==-1)  {
								 e1= edge_order[f_appear[edge_list[edge_order[q]*k+i]]];
							   }else if (e1 != f_appear[edge_list[edge_order[q]*k+i]]){
								 e2= edge_order[f_appear[edge_list[edge_order[q]*k+i]]];
								} //a rule-2 edge can have two possible   situations
							    //i) the node that does not participate has d>2, in which case e1 will be -1
							    //ii) it has d=2, in which case we have e1, e2 hence the need to check both scenarios
							}
					   }
					}
					if (i!=2){ //and after two normally the scaling-process
						temp_val *= (*coeffs)[edge_list[edge_order[q]*k+i]]; //if different dimension than scaling, multiply
					}
				}
				S[edge_list[edge_order[q]*k+2]]+=temp_val;  //add to sum
				if (count_d2 >= k-1){  // first of all  we need to have  >=(k-1)
					if (e1!=-1){ //sanity check, maybe we have to wait for the next turn
						count_eq=0;
						count_exeq=0;
						for (i=0;i<k;++i){
							if (edge_list[edge_order[q]*k+i] == edge_list[e1*k+i] && degs[edge_list[edge_order[q]*k+i]]==2){
								count_eq++; //increase if u_i= v_i and d[u_i]= 2 i.e if rule-2 satisfied for dimension i
							}else if (edge_list[edge_order[q]*k+i] !=  edge_list[e1*k+i] && degs[edge_list[edge_order[q]*k+i]]==1 && degs[edge_list[e1*k+i]]==1){
								count_eq++;
							}if (edge_list[edge_order[q]*k+i] == edge_list[e1*k+i]) {
								count_exeq++;
							}
						}
						if (count_eq>= k - 1 && count_exeq!=k){ //if rule-2 satisfied for all (now needs to be >= instead of = , bc perhaps the all nodes are either  of degree 2 or 1
							*h_pos1=e1; //find the two edges to merge
							*h_pos2=edge_order[q];
							break;
						}
					}
					if (e2!=-1){
						count_eq=0;
						count_exeq=0;
						for (i=0;i<k;++i){
							if (edge_list[edge_order[q]*k+i] == edge_list[e2*k+i] && degs[edge_list[edge_order[q]*k+i]]==2){
								count_eq++; //increase if u_i= v_i and d[u_i]= 2 i.e if rule-2 satisfied for dimension i
							}else if (edge_list[edge_order[q]*k+i] !=  edge_list[e2*k+i] && degs[edge_list[edge_order[q]*k+i]]==1 && degs[edge_list[e2*k+i]]==1){
								count_eq++;
							}if (edge_list[edge_order[q]*k+i] == edge_list[e2*k+i]) {
								count_exeq++;
							}
						}
						if (count_eq>= k - 1 && count_exeq!=k){ //if rule-2 satisfied for all
							*h_pos1=e2; //find the two edges to merge
							*h_pos2=edge_order[q];
							break;
						}
					}


				}
			}
	 if (*h_pos1==-1){
			for (i= d1_starts; i<=d1_ends;++i)
				(*coeffs)[order[i]] =  (max_ni / (d1_ends - d1_starts + 1) )/ S[order[i]];
	 }
}
void  step_preprocess(int k, int *inactive, int **dim_starts,int **dim_ends, int **order, double **coeffs, int **f_appear,int *max_ni,int *degs){
	int dim_q,order_b,order_e,order_t,x,nj;
	*max_ni=0;
	for (dim_q=0;dim_q<k;++dim_q) {
				order_b =  (*dim_starts)[dim_q]; //find where each dimension begins
				order_e=   (*dim_ends)[dim_q];   //and where it finishes
				order_t =order_b;
				while (order_t <=order_e){
					x =(*order)[order_t];
					if (inactive[x] != -1){ //if node is matched, replace it with the last to preserve the order
						(*order)[order_t]=(*order)[order_e];
						order_e--;
					}else{  //otherwise
						order_t++;
						if (dim_q > 0 )
							(*coeffs)[x] = 1;
						else
							(*coeffs)[x] =  (1.0 / degs[x]);
						(*f_appear)[x]= - 1; //no-appearance yet
					}

				}
				(*dim_ends)[dim_q]=order_e; //update dim_ends
				nj =  order_e - order_b + 1;
				if (nj >= *max_ni) *max_ni=nj; //find the max {nj} used for scaling purposes
			}
			
}
//This is the main driver function which applies
int  pm_drive(int *e_list,int *edge_order,int n,int m,int k,int **matched,int *degs,int *dim_starts,int *dim_ends,int sc,int **match_set) {
  int h_pos1,h_pos2,q,i,j;
  int *order;
  int min_ni=n;
  double m_val;
  int ea,eb;
  int can_found=0;
  int ra,rb;
  int rule1_nums;
  int ta,tb,r2_a,r2_b;
  int matches=0;
  int s_matches=0;
  int *edge_list;
  int r2_r,rt1,rt2;
  int deg_limit;
  int *inactive = malloc (n * sizeof(int));
  double *coeffs=malloc(n * sizeof(double));
  double *S=malloc(n * sizeof(double));
   int  *f_appear=malloc(n * sizeof(int));
  int *S_stack= malloc ( (4*n +   3* m )*sizeof(int));
  int c_stack=0;
  int *edge_ids;
  int max_ni;
  edge_list=malloc( k*m * sizeof(int));
  for (i=0;i<m;++i){
	for (j=0;j<k;++j){
		edge_list[i*k+j]=e_list[i*k+j];
	}
  }
  for (i=0;i<n;++i) {
	  coeffs[i]=1.0; //initialize coefficients to 1 for once
	  (*matched)[i]=-1;
	  inactive[i]=-1;
  }
  order=malloc( n * sizeof(int));
 for (i=0;i<k;++i) {
 	if (min_ni > (dim_ends[i] - dim_starts[i] +1) ){
 		min_ni=	(dim_ends[i] - dim_starts[i] +1);
	}
}
edge_ids = malloc(min_ni * sizeof(int));
 for (i=0;i<n;++i) order[i]=i;
		rt1=0;
		rt2=0;
		for (q=1;q<=min_ni;++q){
			if (m==0) break;
			 step_preprocess(k,inactive, &dim_starts,&dim_ends, &order, &coeffs, &f_appear,&max_ni, degs);
			 //first check for reduction-1
			 deg_limit=0;
			 ks_red_1(edge_list, edge_order, &coeffs,m,k,&inactive,matched, order,degs,dim_starts[1],dim_ends[1],&rule1_nums, S,max_ni, &edge_ids,&deg_limit);
			 if (rule1_nums > 0){ //if possible
					rt1 += rule1_nums; //increase the counter
					matches+= rule1_nums;
					for (i=0; i<rule1_nums;++i){
						(*match_set)[s_matches]=edge_ids[i]; s_matches++;
					}
					split_ijz(edge_list,&edge_order,&m,(*matched),k,&degs,deg_limit); //and update the edges
			 }else{ 
				ks_red_2(edge_list, edge_order, &coeffs,m,k,order,degs,dim_starts[2],dim_ends[2],&h_pos1,&h_pos2, S,max_ni,f_appear);
				if (h_pos1 == -1 ){ //no  deg-2 reduction
					sk_one(edge_list, edge_order, &coeffs,m,sc-3,&m_val, &h_pos1,k,degs,max_ni, S,3,dim_starts,dim_ends,order);
					if (h_pos1 == -1){
						break;
					}else{
						matches++;
						deg_limit=0;
						for (i=0;i<k;++i){
							inactive[edge_list[h_pos1*k+i]]=1;
							(*matched)[edge_list[h_pos1*k+i]]=h_pos1;
							deg_limit += degs[edge_list[h_pos1*k+i]];
						}
						split_ijz(edge_list,&edge_order,&m,(*matched),k,&degs,deg_limit); //and update the edges
						(*match_set)[s_matches]= h_pos1;
						s_matches++;
				}
				}else{ //deg-2
						r2_a=-1;
						r2_b=-1;
						r2_r=-1;
						rt2++;
						deg_limit=0;
						//matched nodes
						for (i=0;i<k;++i){
							ta=edge_list[h_pos1*k+i];
							tb=edge_list[h_pos2*k+i];
							deg_limit += degs[ta];
							inactive[ta]=1; //signal that ta is matched (this doesn't mean anything, just removes them from the order array in _sk)
							inactive[tb]=1; //same
							if (ta==tb){
								if (degs[ta]>2){ //same node
									r2_a=ta;
									r2_b=tb;
									r2_r=i;
								}
							}else{
								if (degs[ta]!=degs[tb] || (degs[ta]==degs[tb] && degs[ta]>=2))	{ //the j node
									r2_a=ta;
									r2_b=tb;
									r2_r=i;
								}
							}
					}
					if (r2_a==r2_b && r2_a != -1){ //same-node case, we have the same final node in both edges. No matter which we match, all remaining nodes will be left unmatched
						for (i=0;i<k;++i){
								(*matched)[edge_list[h_pos1*k+i]] = h_pos1;
						}
						split_ijz(edge_list,&edge_order,&m,(*matched),k,&degs,deg_limit); //and update the edges
						(*match_set)[s_matches]=h_pos1; //add one of the two edges to the match_set
						s_matches++;

					}else{
						if (r2_r != -1){ //in this case, we have found two nodes which have a different degree at the corresponding step ==> must be the nodes for index j
							if (degs[r2_b] > degs[r2_a]){ //try to save as little in memory as possible
								inactive[r2_b]=-1; //in actuallitym tb will continue remaining unmatched, as we will transfer to it edges of tb
								split_ijz_2(&edge_list,&edge_order,&m,h_pos1,h_pos2,k,&degs,r2_a,r2_b,r2_r,order,dim_starts,dim_ends,&S_stack,&c_stack);
							}else{
								inactive[r2_a]=-1;
								split_ijz_2(&edge_list,&edge_order,&m,h_pos2,h_pos1,k,&degs,r2_b,r2_a,r2_r,order,dim_starts,dim_ends,&S_stack,&c_stack);
							}
						}else{ //other scenario: all nodes remaining have deg-1  or deg-2:
							//by the conditions of lemma-2 there must exist a deg-2 vertice
							for (i=0;i<k;++i){
								(*matched)[edge_list[h_pos1*k+i]] = h_pos1;
							}
							split_ijz(edge_list,&edge_order,&m,(*matched),k,&degs,deg_limit); //and update the edges
							(*match_set)[s_matches]=h_pos1;
							s_matches++;

					}
				}
				matches++;
				}
			 }
			 }

   c_stack--;
   while (c_stack > 0 ) {
			can_found=-1;
			rb = S_stack[c_stack]; c_stack--;
			eb = S_stack[c_stack]; c_stack--;
			ea = S_stack[c_stack]; c_stack--;

		if  ((*matched)[rb] == -1){ //rb was not matched
			while (S_stack[c_stack]  >= 0 ){
				c_stack--; // no  further  work in here, read a bunch of useless & unmatched edges
			}
			(*match_set)[s_matches] = ea; //match it to either of the two edges
			s_matches++;
			for (i=0;i<k;++i){ //update corresponding entries in the  matched edge (this is important for merged recursions)
				(*matched)[edge_list[ea*k+i]]=  ea;
			}
		}else{ // edge has been matched, must find out from which side the matched vertice came
			while (S_stack[c_stack] >= 0){
				if (S_stack[c_stack] == (*matched)[rb]){
						can_found=1;
				}
				c_stack--;
			}
			ra = edge_list[ea*k+-S_stack[c_stack] - 1];
			if (can_found > 0){ //two-cases, either rb is matched  with edge from ra
				(*matched)[ra]= (*matched)[rb]; //1. do the replace from b back to a
				for (i=0;i<k;++i){
					(*matched)[edge_list[eb*k+i]]=eb; //2.  set each endpoint from eb as matched
				}
				(*match_set)[s_matches]=eb; //add to match_set
				s_matches++;
			}else{ //or not
				for (i=0;i<k;++i){
					(*matched)[edge_list[ea*k+i]]=ea; // set each from ea as matched
				}
				(*match_set)[s_matches]=ea; //add to match_set
				s_matches++;
			}

		}
		c_stack--;
   } 
  //rearrange matched array so that vertices point to other vertices and not edges
 for (i=0;i<s_matches;++i){
	j=(*match_set)[i];	
	for (int z=0;z<k-1;++z){
		(*matched)[e_list[j*k+z]]=e_list[j*k+z+1];	
	}
	(*matched)[e_list[j*k+k-1]]=e_list[j*k];
 }
 free(edge_list);
 free(order);
 free(coeffs);
 free(S_stack);
 free(inactive);
 free(f_appear);
 free(S);
 return matches;
}
