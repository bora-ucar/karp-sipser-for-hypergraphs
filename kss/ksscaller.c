#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include "kss_utils.h"
#define MAXCHAR 100

double u_seconds(void)
{
    struct timeval tp;
    
    gettimeofday(&tp, NULL);
    
    return (double) tp.tv_sec + (double) tp.tv_usec / 1000000.0;
    
}
int verify_matching(int n,int *match,int nmatches,int k,int *edge_list,int *match_set)
{	
	int correct=1;
	int  nmatch=0;
	int *count_matches=calloc(n,sizeof(int));
	for (int i=0;i<n;++i){
		int x=match[i];
		if (x>=0){
			nmatch++;
			count_matches[x]++;		
			if (count_matches[x]==2){
				correct=0;
				break;							
			}	
		}	
	}
	if (correct && nmatch/k==nmatches){
		for (int i=0;i< nmatches;++i){
			for (int z=0;z<k;++z){
				count_matches[edge_list[match_set[i]*k+z]]--;
				if (count_matches[edge_list[match_set[i]*k+z]]<0){
					correct=0;
					break;	
				}	
			}
		}						
	}else{
		correct=0;
	}
	free(count_matches);
	return correct;
}

int test_kss(char *s,int o,int sc,int **match,int *nov,int *matches){
	double t0,t1;	
	int m,n,k,i,correct_match=1;
	 int *edge_list;
	 int *degs;
	int *match_set;
	int *edge_order;
	int ans;
  	int *dim_starts;
	int *dim_ends;
	int  min_ni=-1;
	t0=u_seconds();
	read_tensor_kss(s,&n,&m, &k,&edge_list,&edge_order,&degs,&dim_starts,&dim_ends,o);	
	*match=malloc( n * sizeof(int));
	min_ni= dim_ends[0] - dim_starts[0] + 1;
	for (i=1;i<k;++i){
		if (min_ni  > dim_ends[i] - dim_starts[i]  + 1)	
			min_ni = dim_ends[i] - dim_starts[i]  + 1;

	}
	match_set= malloc (min_ni * sizeof(int));
		
	ans= pm_drive(edge_list,edge_order,n,m,k,match,degs,dim_starts,dim_ends,sc,&match_set);
	
	t1=u_seconds();
	printf("Karp-Sipser-Scaling: matching found: %d, Time elapsed: %.4f seconds \n",ans, (t1-t0));

	correct_match=verify_matching(n,*match,ans, k,edge_list,match_set);
	if (!correct_match){
		printf("Matching is not correct \n");	
	}
	*matches=ans;
	free(degs);
	free(match_set);
	free(edge_list);
	free(edge_order);
	free(dim_starts);
	free(dim_ends);
	return ans;
}

int main(int argc, char **argv)
{
	char *filename=argv[1];
	int o=atoi(argv[2]);
    int sc=atoi(argv[3]);
	srand(time(NULL));
	int *match,n,matches;	
	
	test_kss(filename,o,sc,&match,&n,&matches);
	
	free(match);
	return 0;
}
